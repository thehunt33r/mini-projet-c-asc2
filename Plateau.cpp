/* 
 * File:   Plateau.cpp
 * Author: thehunt33r
 * 
 * Created on February 12, 2015, 2:45 PM
 */
#include <iostream>
#include "Plateau.h"
#include <assert.h>

using namespace std;

Plateau::Plateau(int p_dim):dim(p_dim){ //Initialisation du tableau
    try{ //try if dim is positive and even
        if (dim <= 0)
        {
            throw "Veuillez entrer un nombre positif et pair";
        }
        if (dim%2==0)
        {
            throw "Veuillez entrer un nombre pair";
        }
    }
    catch (int i)
    {
        cerr<<"Useless";
    }
    gameboard = new Case*[dim];
    for(int i = 0; i < dim; i++){
        gameboard[i] = new Case[dim];
    }
    for(int i = 0; i < dim; i++){
        for(int j=0; j < dim; j++){
            if(i == (int)dim/2 && j == (int)dim/2)
            {
                gameboard[i][j] = Case(false, false);
            }
            else {
                gameboard[i][j] = Case();
            }
        }
    }
}

Plateau::Plateau(const Plateau& orig) {
}

Plateau::~Plateau() {
}

void Plateau::affiche() { //On affiche le plateau
    cout << "   ";
    for (int i = 0; i < dim; i++){
        if(i < 10)
        {
            cout << i << " ";
        }
        else
        {
            cout << i << "";
        }
    }
    cout << endl;
    for(int i = 0; i < dim; i++){
        if(i < 10)
        {
            cout << i << "  ";
        }
        else
        {
            cout << i << " ";
        }
        for(int j = 0; j < dim; j++){
            if(gameboard[i][j].isBroken())
            {
                cout<<"x ";
            }
            else if(!(gameboard[i][j].isFree()))
            {
                cout<<"o ";
            }
            else
                cout<<". ";
        }
        cout<<endl;
    }
}

bool Plateau::isCaseBroken(int p_x, int p_y){ 
    assert(p_x < dim && p_x >= 0 && p_y < dim && p_y >= 0); //On vérifie quand même que la case soit dans le tableau
    if(gameboard[p_x][p_y].isBroken())
    {
        return true;
    }
}

bool Plateau::isCaseFree(int p_x, int p_y){
    assert(p_x < dim && p_x >= 0 && p_y < dim && p_y >= 0); //On vérifie quand même que la case soit dans le tableau
    if(gameboard[p_x][p_y].isFree())
    {
        return true;
    }
}

int Plateau::getDimension(){
    return dim;
}
void Plateau::setCase(int p_x, int p_y, bool broken, bool free){
    gameboard[p_x][p_y].setBroken(broken);
    gameboard[p_x][p_y].setFree(free);
    
}
Case Plateau::getCase(int p_x, int p_y){ //Retourne une case
    return gameboard[p_x][p_y];
}
Case::Case(bool p_broken, bool p_free): broken(p_broken),free(p_free){
}

Case::Case(const Case& orig){
}

Case::~Case(){
}

void Case::setBroken(bool p_broken){
    broken = p_broken;
}

void Case::setFree(bool p_free){
    free = p_free;
}

bool Case::isBroken(){
    return broken;
}

bool Case::isFree(){
    return free;
}

