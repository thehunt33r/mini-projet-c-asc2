/* 
 * File:   Partie.h
 * Author: thehunt33r
 *
 * Created on February 23, 2015, 10:51 AM
 */

#ifndef PARTIE_H
#define	PARTIE_H
#include "Plateau.h"
#include "Joueurs.h"
class Partie {
public:
    Partie(int p_dim = 35, string p_pseudo1 = "Ange", string p_pseudo2 = "Demon");
    Partie(const Partie& orig);
    virtual ~Partie();
    void tourdeJeu();
    void getAngel();
private:
    Plateau plateau;
    Joueur* joueurs[2];
};

#endif	/* PARTIE_H */

