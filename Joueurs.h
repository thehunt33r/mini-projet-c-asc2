/* 
 * File:   Joueurs.h
 * Author: thehunt33r
 *
 * Created on February 12, 2015, 3:23 PM
 */

#ifndef JOUEURS_H
#define	JOUEURS_H
#include <utility>
#include <iostream>
#include <cstring>
#include <tuple>
#include <limits>
#include "Plateau.h"
using namespace std;

class Joueur {
public:
    Joueur();
    Joueur(const Joueur& orig);
    virtual ~Joueur();
    std::pair<int,int> saisir();
    virtual bool incorrect(int x, int y, Plateau& plateau) = 0;
    std::pair <int,int> choisirCase(Plateau& plateau);
    virtual void modifierCase(std::pair<int,int> coords, Plateau& plateau) = 0;
    void jouer(Plateau& plateau);
    string getPseudo();
protected:
    string pseudo;

};
class Ange : public Joueur {
public:
    Ange(string p_pseudo, int p_ligne, int p_colonne);
    Ange(const Ange& orig);
    virtual ~Ange();
    void modifierCase(std::pair<int,int> coords, Plateau& plateau) override;
    bool incorrect(int x, int y,Plateau& plateau) override;
    bool coordCorrect(int x, int y);
    int getL();
    int getC();
    void setPos(int x, int y);
    void setL(int p_l);
    void setC(int p_c);
private: 
    int ligne; 
    int colonne ;
};
class Demon : public Joueur{
public:
    Demon(string p_pseudo);
    Demon(const Demon& orig);
    virtual ~Demon();
    bool incorrect(int x, int y, Plateau& plateau) override;
    void modifierCase(std::pair<int,int> coords, Plateau& plateau) override;
};

class AngePuissant : public Ange {
public:
    AngePuissant(string p_pseudo, int p_ligne, int p_colonne, int p_power);
    AngePuissant(const AngePuissant& orig);
    virtual ~AngePuissant();
    bool incorrect(int x, int y,Plateau& plateau) override;
private:
    string pseudo;
    int power;
    int ligne;
    int colonne;
};

#endif	/* JOUEURS_H */

