/* 
 * File:   Partie.cpp
 * Author: thehunt33r
 * 
 * Created on February 23, 2015, 10:51 AM
 */

#include "Partie.h"

Partie::Partie(int p_dim, string p_pseudo1, string p_pseudo2) {
    plateau = Plateau(p_dim);
   joueurs[0] = new Ange(p_pseudo1, (int)p_dim/2, (int)p_dim/2); //initialisation de l'ange et du démon
//   joueurs[0] = new AngePuissant(p_pseudo1, (int)p_dim/2, (int)p_dim/2, 5);
    joueurs[1] = new Demon(p_pseudo2);
}

Partie::Partie(const Partie& orig) {
}

Partie::~Partie() {
}

void Partie::getAngel(){
    auto angel = static_cast<Ange *>(joueurs[0]); //Fonction de debug. Celle-ci nous caste l'ange en Ange* avant de nous donner ses coordonnées
    cout << "L : " << angel->getL()<< endl;
    cout << "C : " << angel->getC() << endl;
}

void Partie::tourdeJeu(){
    string pseudo1 = joueurs[0]->getPseudo(); // On assigne les variables des pseudos
    string pseudo2 = joueurs[1]->getPseudo();
    bool finished = false; //On rentre dans le jeu
    while(!finished){ 
        auto finAngel = [=] (){ //Lambda qui vérifie si l'ange est au bord du plateau 
            auto ange = static_cast<Ange *>(joueurs[0]);
            if(ange->getL() == 0 || ange->getC()==0 || ange->getL()==plateau.getDimension()-1 || ange->getC()==plateau.getDimension()-1){
                cout << "Bravo à l'ange ( "<< pseudo1 <<"), il a gagne en atteignant le bord !";
                return true;
            }
            else{
                return false;
            }
        };
        auto finDemon = [=] (){ //Lambda qui vérifie si le démon a emprisonné l'ange
            auto ange = static_cast<Ange *>(joueurs[0]); 
            int l = ange->getL();
            int c = ange->getC();
            auto nope = [=] (int l, int c){ //Lambda qui nous permet de vérifier si l'ange est emprisonné tout autour de lui
                if(plateau.isCaseBroken(l,c) || !(plateau.isCaseFree(l,c)))
                {
                    return true;
                }
                else{
                    return false;
                }
            };
            if(nope(l-1,c) && nope(l+1,c) && nope(l,c+1) && nope(l,c-1) && nope(l-1,c-1) && nope(l-1,c+1) && nope(l+1,c-1) && nope(l-1,c-1)) { //Conditionnelle : Si toutes les cases autour de l'ange sont cassées et pas occupées, on retourne true
                cout << "Le démon ("<<pseudo2<<" ) a gagné en emprisonnant l'ange! " << endl;
                return true;
            }
            else{
                return false;
            }
        };
        plateau.affiche();
        cout << "Au tour de l'ange ("<< pseudo1<<")" << endl; //tour de l'ange
        joueurs[0]->modifierCase(joueurs[0]->choisirCase(plateau),plateau); //l'ange joue
        if(finAngel()) //On vérifie si l'ange a gagné
        {
            finished = true; //même si c'est inutile, on set finished à true
            break;
        }
        #ifdef _WIN32
             system("cls");
        #elif __linux__
            system("clear");
        #endif
        plateau.affiche();
        cout << "Au tour du démon ("<< pseudo2<<")" << endl;
        joueurs[1]->modifierCase(joueurs[1]->choisirCase(plateau),plateau); //le démon joue
        if(finDemon()) // on vérifie si le démon a gagné
        {
            finished = true; //même si c'est inutile, on set finished à true
            break;
        }
        #ifdef _WIN32
            system("cls");
        #elif __linux__
            system("clear");
        #endif        
    }
}
