/* 
 * File:   Plateau.h
 * Author: thehunt33r
 *
 * Created on February 12, 2015, 2:45 PM
 */

#ifndef PLATEAU_H
#define	PLATEAU_H

class Case; //class signature
class Plateau {
public:
    Plateau(int p_dim = 35);
    Plateau(const Plateau& orig);
    virtual ~Plateau();
    void affiche();
    bool isCaseBroken(int p_x, int p_y);
    bool isCaseFree(int p_x, int p_y);
    int getDimension();
    void setCase(int p_x, int p_y,bool broken, bool free);
    Case getCase(int p_x, int p_y);
private:
    Case** gameboard;
    int dim;
//TO DO : Get/Set pour une case
};

class Case {
public:
    Case(bool p_broken = false ,bool p_free = true);
    Case(const Case& orig);
    virtual ~Case();
    void setBroken(bool p_broken);
    void setFree(bool p_free);
    bool isBroken();
    bool isFree();
private:
    bool broken;
    bool free;
};



#endif	/* PLATEAU_H */

