/* 
 * File:   main.cpp
 * Author: thehunt33r
 *
 * Created on February 12, 2015, 2:26 PM
 */

#include <cstdlib>
#include <iostream>
#include "Plateau.h"
#include "Joueurs.h"
#include "Partie.h"
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
#ifdef _WIN32
    system("cls");
#elif __linux__
    system("clear");
#endif
    int p_dim;
    string p_string1;
    string p_string2;
    cout << "                          Jeu d'Ange et Démon                         \n\n\n\n\n\n\n" << endl;
    cout << "Choisissez la taille de votre plateau : (nombre impair positif) " ;
    cin >> p_dim ;
    cout << "\n" << "Entrez le pseudo de l'ange : ";
    cin >> p_string1;
    cout << "\n" << "Entrez le pseudo du démon : " ;
    cin >> p_string2;
    Partie jeu = Partie(int(p_dim),p_string1,p_string2);
    jeu.tourdeJeu();
    return 0;
}

