/* 
 * File:   Joueurs.cpp
 * Author: thehunt33r
 * 
 * Created on February 12, 2015, 3:23 PM
 */

#include "Joueurs.h"
#include "Plateau.h"
using namespace std;

Joueur::Joueur() {
}

Joueur::Joueur(const Joueur& orig) {
}

Joueur::~Joueur() {
}

std::pair <int,int> Joueur::saisir(){
    int x,y = 0;
    auto ask_value = [=](int& out, std::string message) { //Lambda pour demander la valeur d'une case
        for (;;) {
            std::cout << message;
            if (!(std::cin >> out)) {
                std::cout << "Veuillez entrer une coordonnée.\n";
                std::cin.clear();                                                           
                std::cin.ignore('\n', std::numeric_limits<std::streamsize>::max());
                continue;
            }
 
            else if (out < 0) {
                std::cout << "Veuillez entrer une coordonnée\n";
                continue;
            }
            break;
        }
    };
 
    ask_value(x,  "X: ");
    ask_value(y,  "Y: ");
    return std::make_pair(x, y);   //On retourne les deux valeurs saisies
}

std::pair <int,int> Joueur::choisirCase(Plateau& plateau){
    int x=0;
    int y=0;
    do{
    std::tie(x, y) = saisir(); 
    } while(incorrect(x,y,plateau));
    return std::make_pair(x,y);
}

void Joueur::jouer(Plateau& plateau){
    modifierCase(choisirCase(plateau), plateau);
}

string Joueur::getPseudo(){
    return pseudo;
}

Ange::Ange(string p_pseudo, int p_ligne, int p_colonne){
    pseudo = p_pseudo;
    ligne = p_ligne;
    colonne = p_colonne;
}

Ange::Ange(const Ange& orig){
}

Ange::~Ange(){
}

bool Ange::incorrect(int x, int y, Plateau& plateau){
    int p_dim = plateau.getDimension();
//    auto coordCorrect = [] (int x, int y, int l, int c) {
//        if((x == l-1 && y == c) || (x == l+1 && y == c) || (x == l && y == c+1) || (x == l && y == c-1) || (x == l-1 && y == c-1) || (x == l-1 && y == c+1) || (x == l+1 && y == c-1) || (x == l-1 && y == c-1)){
//            return true;
//        }
//        else if (x==l && y == c){
//            cout << "You have to move" << endl;
//        }
//        else{
//            cout << "You have no power here" << endl;
//            return false;
//        }
//    }; Deprecated 
    auto coordCorrect = [] (int x, int y, int l, int c) { //Lambda qui vérifie si l'ange a le droit/pouvoir de se déplacer dans les cases autour de lui (ici power = 1, donc on regarde les 8 cases directement en contact avec l'ange)
        if(x >= l-1 && x<= l+1 && y>= c-1 && y<= c+1 && !(x==l && y==c)){
            return true;
        }
        else if (x==l && y == c){
            cout << "You have to move" << endl;
            return false;
        }
        else{
            cout << "You have no power here" << endl;
            return false;
        }
    };
    if((x >= 0 && x <= p_dim && y >=0 && y <= p_dim) && plateau.isCaseBroken(x,y)==false && plateau.isCaseFree(x,y) && coordCorrect(x,y,getL(),getC())) //On vérifie que la destination soit dans le tableau, pas cassée, libre, et que les coordonnées soient correctes
    {
        return false;
    }
    else{
        cout << "La case est occupée ou inexistante" << endl;
        return true;
    }
}

void Ange::modifierCase(std::pair<int,int> coords,Plateau& plateau){ //Dans cette méthode, on passe la case de destination en occupée, la case de départ en libre, et on modifie les attributs de Ange
    int x = 0 ;
    int y = 0 ;
    std::tie(x,y) = coords;
    cout << x<<" "<<y<<endl;
    int l = getL();
    int c = getC();
    plateau.setCase(l,c,false,true);
    plateau.setCase(x,y,false,false);
    setL(x);
    setC(y);
}
void Ange::setPos(int x, int y){
    ligne = x;
    colonne = y;
}
int Ange::getL(){
    return ligne;
}
int Ange::getC(){
    return colonne;
}

void Ange::setL(int p_l){
    ligne = p_l;
}

void Ange::setC(int p_c){
    colonne = p_c;
}

Demon::Demon(string p_pseudo){
    pseudo = p_pseudo;
}

Demon::Demon(const Demon& orig){
}

Demon::~Demon(){
}

bool Demon::incorrect(int x, int y, Plateau& plateau){
    int p_dim = plateau.getDimension();
    if((x >= 0 && x <= p_dim && y >=0 && y <= p_dim) && (!(plateau.isCaseBroken(x,y)) || plateau.isCaseFree(x,y))) //Si la case est dans le tableau, qu'elle n'est pas cassée et qu'elle est libre, on retourne true
    {
        return false;
    }
    else{
        return true;
    }
}

void Demon::modifierCase(std::pair<int,int> coords,Plateau& plateau){
    int x = 0 ;
    int y = 0 ;
    std::tie(x,y) = coords;
    cout << x<<" "<<y<<endl;
    plateau.setCase(x,y,true,false); //On casse la case aux coordonnées x,y.
}

//AngePuissant::AngePuissant(string p_pseudo, int p_ligne, int p_colonne, int p_power){
//    pseudo = p_pseudo;
//    ligne = p_ligne;
//    colonne = p_colonne;
//    power = p_power;
//}
//
//AngePuissant::AngePuissant(const AngePuissant&){
//}
//
//AngePuissant::~AngePuissant(){
//}

bool AngePuissant::incorrect(int x, int y,Plateau& plateau){ //Incorrect pour l'ange puissant où on utilise power pour définir la quantité de cases qu'il peut traverser
    int p_dim = plateau.getDimension();
    auto coordCorrect = [] (int x, int y, int l, int c, int power) {
        if(x >= l-power && x<= l+power && y>= c-power && y<= c+power && !(x==l && y==c)){
            return true;
        }
        else if (x==l && y == c){
            cout << "You have to move" << endl;
            return false;
        }
        else{
            cout << "You have no power here" << endl;
            return false;
        }
    };
    if((x >= 0 && x <= p_dim && y >=0 && y <= p_dim) && plateau.isCaseBroken(x,y)==false && plateau.isCaseFree(x,y) && coordCorrect(x,y,getL(),getC(), power))
    {
        return false;
    }
    else{
        return true;
    }
}




